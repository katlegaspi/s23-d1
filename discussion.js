// CRUD Operations
/*
	Create - Insert
	Read - Find
	Update - 
	Delete - 
*/

// Inserting Documents (Create)
// Syntax: db.collectionName.insterOne({object});
// Javascript: object.object.method({object});

db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
                "phone": "09196543210",
                "email": "janedoe@mail.com"
            },
        "courses": ["CSS", "Javascript", "Python"],
        "department": "none"
    });

// Insert Many
// Syntax: db.collectionName.insterMany([{objectA},{objectB}]);

db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
                "phone": "09179876543",
                "email": "stephenhawking@mail.com"
            },
        "courses": ["React", "PHP", "Python"],
        "department": "none"
},
{
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
                "phone": "09061234567",
                "email": "neilarmstrong@mail.com"
            },
        "courses": ["React", "Laravel", "SASS"],
        "department": "none"
}
]);


// Finding documents (Read) Operation
/* Find Syntax:
	db.collectionName.find(); -> finds every document in your collection
	db.collectionName.find({field: value}); -> finds specific field
*/
db.users.find();
db.users.find({"lastName": "Doe"});
db.users.find({"lastName": "Doe", "age": 25}).pretty();

// Updating Documents (Update) Operation
// Syntax: db.collectionName.updateOne({criteria}, {$set: {field: value}}); - updates a single document

// Insert test document
db.users.insertOne({
        "firstName": "Test",
        "lastName": "Test",
        "age": 0,
        "contact": {
                "phone": "00000000000",
                "email": "test@mail.com"
            },
        "courses": [],
        "department": "none"
});

// Update one document
db.users.updateOne(
        { "firstName": "Test" },
        {
            $set: {
            "firstName": "Bill",
            "lastName": "Gates",
            "age": 65,
            "contact": {
                    "phone": "09170123465",
                    "email": "bill@mail.com"
                },
            "courses": ["PHP", "Laravel", "HTML"],
            "department": "none"
            }
        }
);

// Update multiple documents
db.users.updateMany(
    { "department": "none"},
    {
    	$set: { "department": "HR" }
    }
);

// Deleting Documents (Delete) Operation
// Syntax: db.collectionName.deleteOne({criteria}); - deletes a single document

// Create a single document with one field
db.users.insert({
    "firstName": "test"
});

// Delete one document
db.users.deleteOne({
    "firstName": "test"
});

// Update multiple documents where lastName is "Doe"
db.users.updateMany(
    { "lastName": "Doe" },
    {
    	$set: {
    		"department": "Operations"
    	}
    }
);

// Delete multiple documents
db.users.deleteMany(
    { "department": "Operations" }
);

// Advanced Query

// Query an embedded document
db.users.find({
	"contact" : {
    	"phone" : "09170123465",
    	"email" : "bill@mail.com"		
	}
});

// Querying an Array without a specific order of elements
db.users.find({ "courses": { $all: ["React", "Python"]} });
// adding $all will find for "React" and "Python"